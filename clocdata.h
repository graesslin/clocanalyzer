/********************************************************************
 ClocAnalyzer

Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef CLOCANALYZER_DATA_H
#define CLOCANALYZER_DATA_H
#include <QObject>

class QIODevice;
class QXmlStreamAttributes;

namespace ClocAnalyzer {
class DataSet;

class Data : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int fileCount READ fileCount CONSTANT)
    Q_PROPERTY(int totalLineCount READ totalLineCount CONSTANT)
    Q_PROPERTY(int blankCount READ blankCount CONSTANT)
    Q_PROPERTY(int commentCount READ commentCount CONSTANT)
    Q_PROPERTY(int codeCount READ codeCount CONSTANT)
public:
    virtual ~Data();
    int fileCount() const;
    int totalLineCount() const;
    int blankCount() const;
    int commentCount() const;
    int codeCount() const;
protected:
    explicit Data(QObject *parent = 0);
    void setFileCount(int count);
    void setBlankCount(int count);
    void setCommentCount(int count);
    void setCodeCount(int count);
    void parse(const QXmlStreamAttributes &attribs);
private:
    void setValue(const QString &value, void (Data::*function)(int));
    int m_fileCount;
    int m_totalLineCount;
    int m_blankCount;
    int m_commentCount;
    int m_codeCount;
};

class LanguageData : public Data
{
    Q_OBJECT
    Q_PROPERTY(QString language READ language)
public:
    virtual ~LanguageData();

    const QString &language() const;

    static LanguageData *create(const QXmlStreamAttributes &attribs, DataSet *parent);
private:
    explicit LanguageData(DataSet *parent = 0);
    DataSet *m_parentData;
    QString m_language;
};

class DataSet : public Data
{
    Q_OBJECT
public:
    virtual ~DataSet();
    const QString &reportFile() const;
    const QList<LanguageData*> &languages() const;

    static DataSet *create(QIODevice *input, QObject *parent = 0);
private:
    explicit DataSet(QObject *parent = 0);
    QString m_reportFile;
    QList<LanguageData*> m_languages;
};

#define SETTER(name, variableName) \
inline \
void Data::name(int count) \
{ \
    variableName = count; \
}

SETTER(setFileCount, m_fileCount)
SETTER(setBlankCount, m_blankCount)
SETTER(setCommentCount, m_commentCount)
SETTER(setCodeCount, m_codeCount)
#undef SETTER

#define GETTER(name) \
inline \
int Data::name() const \
{ \
    return m_##name; \
}

GETTER(fileCount)
GETTER(blankCount)
GETTER(commentCount)
GETTER(codeCount)

#undef GETTER

inline
int Data::totalLineCount() const
{
    return m_blankCount + m_codeCount + m_commentCount;
}

inline
const QString& LanguageData::language() const
{
    return m_language;
}

inline
const QString& DataSet::reportFile() const
{
    return m_reportFile;
}

inline
const QList< LanguageData* >& DataSet::languages() const
{
    return m_languages;
}

} // namespace

#endif
