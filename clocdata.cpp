/********************************************************************
 ClocAnalyzer

Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "clocdata.h"
// Qt
#include <QDebug>
#include <QIODevice>
#include <QXmlStreamReader>

namespace ClocAnalyzer {

Data::Data(QObject *parent) 
: QObject(parent)
, m_fileCount(0)
, m_totalLineCount(0)
, m_blankCount(0)
, m_commentCount(0)
, m_codeCount(0)
{
}

Data::~Data()
{
}

void Data::setValue (const QString& value, void (Data::*function)(int))
{
    if (value.isEmpty()) {
        return;
    }
    bool ok = false;
    const int converted = value.toInt(&ok);
    if (!ok) {
        return;
    }
    (this->*function)(converted);
}

void Data::parse(const QXmlStreamAttributes& attribs)
{
    QString files;
    if (attribs.hasAttribute("sum_files")) {
        files = attribs.value("sum_files").toString();
    }
    if (attribs.hasAttribute("files_count")) {
        files = attribs.value("files_count").toString();
    }
    setValue(files, &Data::setFileCount);
    setValue(attribs.value("blank").toString(), &Data::setBlankCount);
    setValue(attribs.value("comment").toString(), &Data::setCommentCount);
    setValue(attribs.value("code").toString(), &Data::setCodeCount);
}

DataSet* DataSet::create(QIODevice *file, QObject* parent)
{
    DataSet *dataSet = new DataSet(parent);
    QXmlStreamReader xml(file);
    while (!xml.atEnd()) {
        if (!xml.readNextStartElement()) {
            continue;
        }
        if (xml.name().toString() == "report_file") {
            dataSet->m_reportFile = xml.readElementText();
        }
        if (xml.name().toString() == "total") {
            dataSet->parse(xml.attributes());
        }
        if (xml.name().toString() == "language") {
            dataSet->m_languages << LanguageData::create(xml.attributes(), dataSet);
        }
    }
    return dataSet;
}

DataSet::DataSet(QObject *parent) 
: Data(parent)
{
}

DataSet::~DataSet()
{
}

LanguageData* LanguageData::create(const QXmlStreamAttributes& attribs, DataSet* parent)
{
    LanguageData *data = new LanguageData(parent);
    data->parse(attribs);
    if (attribs.hasAttribute("name")) {
        data->m_language = attribs.value("name").toString();
    }
    return data;
}

LanguageData::LanguageData(DataSet* parent) 
: Data(parent)
, m_parentData(parent)
{
}

LanguageData::~LanguageData()
{
}

} // namespace

#include "clocdata.moc"

