/********************************************************************
 ClocAnalyzer

Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "version.h"
#include "clocdata.h"
#include <QDir>
#include <iostream>

namespace ClocAnalyzer {

Version::Version (QObject* parent)
: QObject (parent)
{
}

Version::~Version()
{
}

Version* Version::create(const QDir &directory, QObject *parent)
{
    Version *version = new Version(parent);
    version->m_name = directory.dirName();
    for (const QString &fileName : directory.entryList(QStringList() << "*.xml", QDir::Files | QDir::Readable)) {
        QFile file(directory.filePath(fileName));
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            continue;
        }
        version->m_data.insert(fileName.left(fileName.length() - 4), DataSet::create(&file, version));
        file.close();
    }
    return version;
}

void Version::print()
{
    std::cout << "Version: " << m_name.toUtf8().constData() << std::endl;
    for (auto it = m_data.constBegin(); it != m_data.constEnd(); ++it) {
        std::cout << "Directory: " << it.key().toUtf8().constData() << std::endl;
        DataSet *dataSet = it.value();
        std::cout << dataSet->reportFile().toUtf8().constData() << std::endl;
        std::cout << dataSet->fileCount() << std::endl;
        std::cout << dataSet->blankCount() << std::endl;
        std::cout << dataSet->commentCount() << std::endl;
        std::cout << dataSet->codeCount() << std::endl;
        std::cout << dataSet->totalLineCount() << std::endl;
        foreach (LanguageData *data, dataSet->languages()) {
            std::cout << data->language().toUtf8().constData() << std::endl;
            std::cout << data->fileCount() << std::endl;
            std::cout << data->blankCount() << std::endl;
            std::cout << data->commentCount() << std::endl;
            std::cout << data->codeCount() << std::endl;
            std::cout << data->totalLineCount() << std::endl;
        }
    }
}

DataSet* Version::dataSet(const QString &name) const
{
    auto it = m_data.find(name);
    if (it == m_data.end()) {
        return NULL;
    }
    return it.value();
}

} // namespace

#include "version.moc"
