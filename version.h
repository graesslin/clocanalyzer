/********************************************************************
 ClocAnalyzer

Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef CLOCANALYZER_VERSION_H
#define CLOCANALYZER_VERSION_H
#include <QObject>
#include <QHash>

class QDir;
namespace ClocAnalyzer {

class DataSet;

class Version : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
public:
    virtual ~Version();
    const QString &name() const;
    void print();
    QList<QString> dataSetNames() const;
    DataSet *dataSet(const QString &name) const;

    static Version *create(const QDir &directory, QObject *parent = 0);
private:
    explicit Version(QObject *parent = 0);
    QString m_name;
    QHash<QString, DataSet*> m_data;
};

inline
const QString& Version::name() const
{
    return m_name;
}

inline
QList<QString> Version::dataSetNames() const
{
    return m_data.keys();
}

}

#endif
