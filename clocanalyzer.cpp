/********************************************************************
 ClocAnalyzer

Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "clocanalyzer.h"
#include "clocdata.h"
#include "version.h"

#include <QTimer>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include <QSet>
#include <iostream>

namespace ClocAnalyzer {

Application::Application(const QString &path)
{
    QTimer* timer = new QTimer(this);
    process(path);
    QTimer::singleShot(0, QCoreApplication::instance(), SLOT(quit()));
}

Application::~Application()
{}

void Application::process(const QString &directory)
{
    QDir dir(directory);
    if (!dir.exists()) {
        return;
    }
    QList<Version*> versions;
    for (const QString &dirName : dir.entryList(QStringList(), QDir::AllDirs | QDir::NoDotAndDotDot)) {
        std::cout << dirName.toUtf8().constData() << std::endl;
        versions << processVersion(dir.filePath(dirName));
    }
    std::sort(versions.begin(), versions.end(), [](Version *v1, Version *v2) {
        auto parts1 = v1->name().split(".");
        auto parts2 = v2->name().split(".");
        const int major1 = parts1.at(1).toInt();
        const int major2 = parts2.at(1).toInt();
        if (major1 == major2) {
            return parts1.at(2).toInt() < parts2.at(2).toInt();
        }
        return major1 < major2;
    });
    QSet<QString> dataSets;
    std::for_each(versions.constBegin(), versions.constEnd(), [&](Version *version) {
        auto versionDataSets = version->dataSetNames();
        std::for_each(versionDataSets.constBegin(), versionDataSets.constEnd(), [&](const QString &data){
            dataSets << data;
        });
    });

    bool addComma = false;
    std::for_each(dataSets.constBegin(), dataSets.constEnd(), [&](const QString &dataSet) {
        if (addComma) {
            std::cout << "," << std::endl;
        }
        int x = 0;
        std::cout << "{ label: \"" << dataSet.toUtf8().constBegin() << "\", data: [";
        std::for_each(versions.constBegin(), versions.constEnd(), [&x, &dataSet](Version *version) {
            if (x > 0) {
                std::cout << ", ";
            }
            std::cout << "[" << x++ << ", ";
            DataSet *data = version->dataSet(dataSet);
            if (data) {
                std::cout << data->totalLineCount() << ", ";
                auto printLineCounts = [](Data *data) {
                    std::cout << "total: "    << data->totalLineCount();
                    std::cout << ", code: "    << data->codeCount();
                    std::cout << ", comment: " << data->commentCount();
                    std::cout << ", blank: "   << data->blankCount();
                };
                std::cout << "{";
                printLineCounts(data);
                std::cout << ", languages: [";
                bool addLanguageComma = false;
                std::for_each(data->languages().constBegin(), data->languages().constEnd(), [&addLanguageComma,&printLineCounts](LanguageData *languageData){
                    if (addLanguageComma) {
                        std::cout << ", ";
                    } else {
                        addLanguageComma = true;
                    }
                    std::cout << "{";
                    printLineCounts(languageData);
                    std::cout << ", language: \"" << languageData->language().toUtf8().constData() << "\"";
                    std::cout << "}";
                });
                std::cout << "]}";
            } else {
                std::cout << "0";
            }
            std::cout << "]";
        });
        std::cout << "]}";
        addComma = true;
    });
}

Version *Application::processVersion(const QString &directory)
{
    QDir dir(directory);
    if (!dir.exists()) {
        return NULL;
    }
    Version *version = Version::create(dir, this);
    return version;
}

} // namespace

#include "clocanalyzer.moc"
