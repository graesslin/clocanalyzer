project(clocanalyzer)
cmake_minimum_required(VERSION 2.6)
find_package(Qt4 REQUIRED)

include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR})

set(clocanalyzer_SRCS clocanalyzer.cpp clocdata.cpp main.cpp version.cpp)
qt4_automoc(${clocanalyzer_SRCS})
add_definitions(-std=gnu++0x)
add_executable(clocanalyzer ${clocanalyzer_SRCS})
target_link_libraries(clocanalyzer ${QT_QTCORE_LIBRARY})

install(TARGETS clocanalyzer RUNTIME DESTINATION bin)
